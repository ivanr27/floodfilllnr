/*
Created on: 20/05/2018
*      Author: Ivan Ruhl  - Grupo de Robotica UTN FRA
*      Brief: Codigo para el control del Robot Kuro
/*


#ifndef floodfill_h
#define floodfill_h

#include <stdint.h>
#include <EEPROM.h>


/* define constants ------------------------------------------------------------------------*/

// Coordenadas de inicio en el mapa
#define POS_X_INI    9					//0
#define POS_Y_INI    9

// Etiquetas de bits de celdas
#define PARED_OESTE   0
#define PARED_ESTE    1
#define PARED_SUR     2
#define PARED_NORTE   3
#define CELDA_INICIAL 4
#define CELDA_FINAL   5

// Etiquetas de la actualizacion de coordenadas
#define DIR_FWD       3
#define DIR_RGT       1
#define DIR_LFT       0
#define DIR_180       4

#define BRJ_FWD       0
#define BRJ_RGT       1
#define BRJ_LFT       2
#define BRJ_180       3

// Macros para manejo de bits
#define BIT(x)            (1<<(x))
#define BIT_GET(x,b)      ((x) & BIT(b))  
#define BIT_SET(x,b)      ((x) |= BIT(b))
#define BIT_CLEAR(x,b)    ((x) &= ~BIT(b))
#define BIT_WRITE(x,b,v)  ((v)? BIT_SET(x,b) : BIT_CLEAR(x,b))


/* Global variables -----------------------------------------------------------------------------*/

extern uint8_t map_state[18][18];			//9,18
extern uint8_t map_value[18][18];			//9,18
extern uint8_t map_less_value[18][18];		//9,18
extern uint8_t pos_X, pos_Y;
extern uint8_t cnt_move;
extern uint8_t optimized_move[50];
extern uint8_t move_ff[30];
extern uint8_t final_move[30];
extern uint8_t tot_cnt_opt_mv;
extern int8_t  brujula, brujula2;


/* Functions ---------------------------------------------------------------------------------------*/

void init_maze(void);  
void debug_maze();  
void floodfill(void);  
void resolver(void);
void save_maze (void);
void read_maze(void);

void mapeo_optimizado(void);

void run_optimized_maze(void);

void end_map(void);

void calc_coord(uint8_t direccion , uint8_t modo);
void actualizar_brujula(uint8_t accion, int8_t cur_bruj);
void has_wall_presence(void);
void draw_walls (uint8_t modo, uint8_t left, uint8_t right, uint8_t front);

void follow_right_wall(void);



#endif

/******* end of file *******/
