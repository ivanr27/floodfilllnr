/*
Created on: 20/05/2018
*      Author: Ivan Ruhl  - Grupo de Robotica UTN FRA
*      Brief: Codigo para el control del Robot Kuro
/*


#include "Floodfill.h"


/* Global variables -----------------------------------------------------------------------------*/

uint8_t map_state[18][18];
uint8_t map_value[18][18];
uint8_t map_less_value[18][18];
static char i, j, estado;
uint8_t pos_X = POS_X_INI;
uint8_t pos_Y = POS_Y_INI;
uint8_t maze_size_X = 18;
uint8_t maze_size_Y = 18;
uint8_t cnt_move;
int8_t brujula2 = 0;
uint8_t optimized_move[50];
uint8_t move_ff[30];
uint8_t final_move[30];
uint8_t tot_cnt_opt_mv;
uint8_t opt_cnt;
int8_t brujula;






//--------------------------------------------------------------------------------------------------

/*
*	Primera vuelta
*	seguidor de pared izquierda con mapeo
*/
void follow_right_wall(void) {
	get_adc();
	has_wall_presence();

	//GIRO DERECHA:
	if (!wall_presence.hasRightWall) {
		giro_derecha();
		avanzar();
		calc_coord(DIR_RGT, brujula);
		actualizar_brujula(BRJ_RGT, brujula);
		has_wall_presence();
		draw_walls(brujula, wall_presence.hasLeftWall, wall_presence.hasRightWall, wall_presence.hasFrontWall);
	}

	//AVANCE RECTO:
	else if (wall_presence.hasRightWall && !(wall_presence.hasFrontWall)) {

		avanzar();
		calc_coord(DIR_FWD, brujula);
		actualizar_brujula(BRJ_FWD, brujula);
		has_wall_presence();
		draw_walls(brujula, wall_presence.hasLeftWall, wall_presence.hasRightWall, wall_presence.hasFrontWall);
	}

	//GIRO IZQUIERDA:
	else if (wall_presence.hasRightWall && wall_presence.hasFrontWall && !(wall_presence.hasLeftWall)) {

		giro_izquierda();
		avanzar();
		calc_coord(DIR_LFT, brujula);
		actualizar_brujula(BRJ_LFT, brujula);
		has_wall_presence();
		draw_walls(brujula, wall_presence.hasLeftWall, wall_presence.hasRightWall, wall_presence.hasFrontWall);
	}

	//GIRO 180:
	else if (wall_presence.hasRightWall && wall_presence.hasFrontWall && wall_presence.hasLeftWall) {
		//fin de mapa:
		if (get_line_sensors()) {
			freno();
			end_map();
			delay(10000);
		}

		//callejon sin salida:
		else {

			giro_180();
			avanzar();
			calc_coord(DIR_180, brujula);
			actualizar_brujula(BRJ_180, brujula);
			has_wall_presence();
			draw_walls(brujula, wall_presence.hasLeftWall, wall_presence.hasRightWall, wall_presence.hasFrontWall);
		}
	}
}


//Segunda vuelta (rapida)
void speedRun(void) {
	read_maze();
	mapeo_optimizado();
	run_optimized_maze();
	stopMotors(10000);
}








/* Functions ---------------------------------------------------------------------------------------*/

/* init_maze
   /completa los campos de cada celda con todas las paredes en 1, los valores en 0 y los menores valores en 100
*/
void init_maze(void) {
	//inicializar celdas de laberinto
	for (int fila = 0; fila < maze_size_X; fila++)
	{
		for (int columna = 0; columna < maze_size_Y; columna++)
		{
			BIT_SET(map_state[fila][columna], PARED_NORTE);
			BIT_SET(map_state[fila][columna], PARED_SUR);
			BIT_SET(map_state[fila][columna], PARED_ESTE);
			BIT_SET(map_state[fila][columna], PARED_OESTE);
			map_value[fila][columna] = 0;
			map_less_value[fila][columna] = 100;
		}
	}
	// primer celda, 3 paredes
	BIT_SET(map_state[POS_X_INI][POS_Y_INI], PARED_NORTE);
	BIT_SET(map_state[POS_X_INI][POS_Y_INI], PARED_SUR);
	BIT_CLEAR(map_state[POS_X_INI][POS_Y_INI], PARED_ESTE);
	BIT_SET(map_state[POS_X_INI][POS_Y_INI], PARED_OESTE);
}


/* debug_maze
   muestra por puerto serie los valores de cada celda
*/
void debug_maze() {
	for (int columna = 0; columna < maze_size_Y; columna++)
	{
		for (int fila = 0; fila < maze_size_X; fila++)
		{
			Serial.print(map_value[fila][columna]);
			Serial.print("  ");
		}
		Serial.println("");
	}
	Serial.println("");
}


/* floodfill
   mientras el valor de la celda inicial sea 0, compara cada celda con sus adyacentes.
   si el valor de la celda actual es menor a la adyacente le asigna su valor correspondiente.
   el valor es igual al menor valor adyacente +1.
*/
void floodfill() {

	while (map_value[POS_X_INI][POS_Y_INI] == 0) {
		for (j = 0; j < maze_size_Y; j++) {
			for (i = 0; i < maze_size_X; i++) {

				if (!(BIT_GET(map_state[i][j], PARED_NORTE)) && map_value[i][j - 1] != 0 && map_value[i][j - 1] < map_less_value[i][j]) {
					map_value[i][j] = map_value[i][j - 1] + 1;
					map_less_value[i][j] = map_value[i][j];
				}

				if (!(BIT_GET(map_state[i][j], PARED_SUR)) && map_value[i][j + 1] != 0 && map_value[i][j + 1] < map_less_value[i][j]) {
					map_value[i][j] = map_value[i][j + 1] + 1;
					map_less_value[i][j] = map_value[i][j];
				}

				if (!(BIT_GET(map_state[i][j], PARED_ESTE)) && map_value[i + 1][j] != 0 && map_value[i + 1][j] < map_less_value[i][j]) {
					map_value[i][j] = map_value[i + 1][j] + 1;
					map_less_value[i][j] = map_value[i][j];
				}

				if (!(BIT_GET(map_state[i][j], PARED_OESTE)) && map_value[i - 1][j] != 0 && map_value[i - 1][j] < map_less_value[i][j]) {
					map_value[i][j] = map_value[i - 1][j] + 1;
					map_less_value[i][j] = map_value[i][j];
				}
			}
		}
		debug_maze();
	}
	if (map_value[POS_X_INI][POS_Y_INI] != 0) {
		i = 0;
		j = 0;
		//estado = 1; //switch de arduino
	}
}


/* resolver
   Empezando por la casilla inicial y hasta llegar a la casilla final,
   se compara el valor de cada casilla con el valor de sus adyacentes;
   se guarda en un vector mov_ff[] la direccion (N,S,E,O) de la casilla adyacente con menor valor;
   se mueve a la direccion (i,j) de dicha casilla y se repite el proceso.
*/
void resolver() {
	int8_t next_move = 5;
	i = POS_X_INI;
	j = POS_Y_INI;
	cnt_move = 0;

	while (!(BIT_GET(map_state[i][j], CELDA_FINAL))) {
		if (map_value[i][j] != 0) {
			if (!(BIT_GET(map_state[i][j], PARED_NORTE)) && map_value[i][j] == map_value[i][j - 1] + 1) {
				next_move = 0;
			}
			if (!(BIT_GET(map_state[i][j], PARED_SUR)) && map_value[i][j] == map_value[i][j + 1] + 1) {
				next_move = 1;
			}
			if (!(BIT_GET(map_state[i][j], PARED_ESTE)) && map_value[i][j] == map_value[i + 1][j] + 1) {
				next_move = 2;
			}
			if (!(BIT_GET(map_state[i][j], PARED_OESTE)) && map_value[i][j] == map_value[i - 1][j] + 1) {
				next_move = 3;
			}

			//norte: estado=0  accion=3
			if (next_move == 0) {
				move_ff[cnt_move] = 3;  //norte == 3
				cnt_move++;

				//sprintf(buf,"Norte\n");
				i = i;
				j = j - 1;
			}

			//sur:
			if (next_move == 1) {
				move_ff[cnt_move] = 1;
				cnt_move++;

				//sprintf(buf,"Sur\n");
				i = i;
				j = j + 1;
			}

			//este
			if (next_move == 2) {
				move_ff[cnt_move] = 0;  //norte == 3
				cnt_move++;

				//sprintf(buf,"Este\n");
				i = i + 1;
				j = j;
			}

			//oeste
			if (next_move == 3) {
				move_ff[cnt_move] = 2;  //norte == 3
				cnt_move++;

				//sprintf(buf,"Oeste\n");
				i = i - 1;
				j = j;
			}
		}
	}
	if ((BIT_GET(map_state[i][j], CELDA_FINAL))) {
		//sprintf(buf,"listo\n");
	}
}


/* save_maze
   Se convierte, considerando la brujula y la direccion, las direcciones del vector mov_ff[] en los respectivos movimientos
   y se los guarda en la memoria no volatil
   en la direccion 0 se guarda la cantidad de elementos del vector mov_ff[]
*/
void save_maze(void) {
	uint8_t cnt;
	brujula2 = 0;
	uint8_t startAddress = 0;

	EEPROM.write(startAddress, cnt_move);
	startAddress = startAddress + sizeof(startAddress);

	for (cnt = 0; cnt < cnt_move; cnt++)
	{
		//switch(brujula2)
		//{
		// modo 0:
		//este   = adelante
		//sur    = derecha
		//oeste  = -
		//norte  = izquierda

		//case 0:
		if (brujula2 == 0)
		{
			switch (move_ff[cnt])
			{
				//este
			case 0:
				EEPROM.write(startAddress, 0);     // 0 = adelante
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2;
				break;

				//sur
			case 1:
				EEPROM.write(startAddress, 1);     // 2 = derecha
				startAddress = startAddress + sizeof(startAddress);
				break;

				//oeste
			case 2:
				EEPROM.write(startAddress, 2);    // 2 = atras
				startAddress = startAddress + sizeof(startAddress);
				break;

				//norte
			case 3:
				EEPROM.write(startAddress, 3);     // 3 = izquierda
				startAddress = startAddress + sizeof(startAddress);
				break;
			}
		}

		// end modo 0

		// modo 1:
		//este   = izq
		//sur    = adel
		//oeste  = der
		//norte  = -

		//case 1:
		if (brujula2 == 1)
		{
			switch (move_ff[cnt])
			{
				//izq
			case 0:   //este
				EEPROM.write(startAddress, 1);
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2 - 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}
				break;

				//avanzar
			case 1:     //sur
				EEPROM.write(startAddress, 0);    // 0 = adelante
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2;
				break;

				//derecha
			case 2:   //oeste
				EEPROM.write(startAddress, 2);     // 2 = derecha
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2 + 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}
				break;

				//nada
			case 3:   //norte
				break;

			}
			//break;
		}
		//end modo 1


		// modo 2:
		//este   = -
		//sur    = izq
		//oeste  = adl
		//norte  = der
		//case 2:
		if (brujula2 == 2)
		{
			switch (move_ff[cnt])
			{
				//nada
			case 0:   //este
				break;

				//izq
			case 1:   //sur
				EEPROM.write(startAddress, 1);    // 1 = izquierda
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2 - 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}
				break;

				//avanzar
			case 2:     //oeste
				EEPROM.write(startAddress, 0);    // 0 = adelante
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2;
				break;

				//derecha
			case 3:   //norte
				EEPROM.write(startAddress, 2);     // 2 = derecha
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2 + 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}
				break;

			}
			//break;
		}
		//end modo 2


		// modo 3:
		//este   = der
		//sur    = -
		//oeste  = izq
		//norte  = adl
		//case 3:
		if (brujula2 == 3)
		{
			switch (move_ff[cnt])
			{

				//derecha
			case 0:   //este
				EEPROM.write(startAddress, 2);     // 2 = derecha
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2 + 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}
				break;

				//nada
			case 1:   //sur
				break;

				//izq
			case 2:   //oeste
				EEPROM.write(startAddress, 1);     // 1 = izquierda
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2 - 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}
				break;

				//avanzar
			case 3:     //norte
				EEPROM.write(startAddress, 0);     // 0 = adelante
				startAddress = startAddress + sizeof(startAddress);
				brujula2 = brujula2;
				break;

			}
			//break;
		}
		//end modo 3


		//}//end switch(brujula2)
	}//end for
}


/* read_maze
   se leen los valores grabados en ROM y se los guarda en un vector de movimientos
   el numero de elementos totales del vector esta grabado en la direccion 0.
*/
void read_maze(void) {
	uint8_t cnt = 0;
	uint8_t cnt_total = 0;
	uint8_t startAddress = 0;

	cnt_total = EEPROM.read(startAddress);
	startAddress = startAddress + sizeof(startAddress);

	for (cnt = 0; cnt < cnt_total; cnt++)
	{
		move_ff[cnt] = EEPROM.read(startAddress);
		startAddress = startAddress + sizeof(startAddress);
		//sprintf(buf,"%d\n", move[cnt]);
	}

	move_ff[0] = 4;
}




//----------------------------------------------------------------------------------------------------------------------------
// Optimizacion de recorrido



/*mapeo optimizado:
  lee el vector de movimientos guardado en flash y guarda en ram un vector con direcciones relativas:
  0 : adelante
  1 : izquierda
  2 : derecha
  3 : media celda
*/
void mapeo_optimizado(void) {
	uint8_t cnt = 0;
	brujula2 = 0;
	uint8_t cnt_total = 0;
	cnt_total = EEPROM.read(0);

	for (cnt = 0; cnt < cnt_total; cnt++)
	{
		if (brujula2 == 0)
		{
			if (move_ff[cnt] == 0) {
				optimized_move[cnt] = 0;  //adelante
				//sprintf(buf,"adl %d\n", brujula2);
			}

			else if (move_ff[cnt] == 1) {
				brujula2 = brujula2 + 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}

				optimized_move[cnt] = 2;
				//sprintf(buf,"der %d\n", brujula2);
			}

			else if (move_ff[cnt] == 3) {
				brujula2 = brujula2 - 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}

				optimized_move[cnt] = 1;
				//sprintf(buf,"izq %d\n", brujula2);
			}

			//avance de media celda
			else if (move_ff[cnt] == 4) {
				optimized_move[cnt] = 3;
				//sprintf(buf,"adl %d\n", brujula2);
			}
		}

		//modo 1
		else if (brujula2 == 1)
		{
			if (move_ff[cnt] == 1) {
				optimized_move[cnt] = 0;
				//sprintf(buf,"adl %d\n", brujula2);
			}

			else if (move_ff[cnt] == 2) {
				brujula2 = brujula2 + 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}

				optimized_move[cnt] = 2;
				//sprintf(buf, "der %d\n", brujula2);
			}

			else if (move_ff[cnt] == 0) {
				brujula2 = brujula2 - 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}

				optimized_move[cnt] = 1;
				//sprintf(buf,"izq %d\n", brujula2);
			}
		}

		//modo 2
		else if (brujula2 == 2)
		{
			if (move_ff[cnt] == 2) {
				optimized_move[cnt] = 0;
				//sprintf(buf,"adl %d\n", brujula2);
			}

			else if (move_ff[cnt] == 3) {
				brujula2 = brujula2 + 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}

				optimized_move[cnt] = 2;
				//sprintf(buf,"der %d\n", brujula2);
			}

			else if (move_ff[cnt] == 1) {
				brujula2 = brujula2 - 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}
				optimized_move[cnt] = 1;
				//sprintf(buf,"izq %d\n", brujula2);
			}
		}

		//modo 3
		else if (brujula2 == 3)
		{
			if (move_ff[cnt] == 3) {
				optimized_move[cnt] = 0;
				//sprintf(buf, "adl %d\n", brujula2);
			}

			else if (move_ff[cnt] == 0) {
				brujula2 = brujula2 + 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}

				optimized_move[cnt] = 2;
				//sprintf(buf, "der %d\n", brujula2);
			}

			else if (move_ff[cnt] == 2) {
				brujula2 = brujula2 - 1;
				if (brujula2 < 0) {
					brujula2 = 3;
				}
				if (brujula2 > 3) {
					brujula2 = 0;
				}

				optimized_move[cnt] = 1;
				//sprintf(buf,"izq %d\n", brujula2);
			}
		}
	}
}




//----------------------------------------------------------------------------------------------------------------------------
// Recorrer laberinto



/* run_optimized_maze
   se asignan las acciones correspondientes (avanzar, girar) para cada elemento del vector de movimiento optimizado
   casos:
   0: giro 90 izq
   1: giro 90 der
   3: primer avance
*/
void run_optimized_maze() {
	uint8_t cnt_run = 0;

	for (cnt_run = 0; cnt_run < tot_cnt_opt_mv; cnt_run++) {			//cnt_run = 0
		switch (final_move[cnt_run]) {

		case 0:
			//90 left turn
			Serial.println("izquierda");
			freno();
			giro_izquierda();
			avanzar();


			break;


			//right turn:
		case 1:
			Serial.println("izquierda");
			freno();
			giro_derecha();
			avanzar();

			break;

		case 3:
			avanzar_mediaCelda();
			break;

		case 4:
		
			Serial.println("celda*1");
			avanzar();
			break;

		

		default:
			//buzzer(beepFinish);
			freno();
			delay(10000);
			break;
		}
	}
}




/*
	setea las variables de valor, menor valor y el flag de fin de mapeo.
	inicia el floodfill, determina las acciones y guarda el array en memoria eeprom.
	indica el fin de la operacion con el led verde y un sonido.
*/
void end_map(void) {
	map_value[pos_X][pos_Y] = 1;
	map_less_value[pos_X][pos_Y] = 1;
	BIT_SET(map_state[pos_X][pos_Y], CELDA_FINAL);

	floodfill();
	resolver();
	save_maze();
}


/* calc_coord
   Actualiza las coordenadas de posicion en el mapa
   direccion: DIR_FWD, DIR_RGT, DIR_LFT, DIR_180
   modo: brujula
*/
void calc_coord(uint8_t direccion, uint8_t modo) {
	//giro a la izquierda
	if (direccion == 0)
	{
		switch (modo) {
		case 0:
		{
			//pos_X ++;
			pos_Y--;
			break;
		}

		case 1:
		{
			pos_X++;
			//pos_Y ++;
			break;
		}

		case 2:
		{
			//pos_X --;
			pos_Y++;
			break;
		}

		case 3:
		{
			pos_X--;
			//pos_Y --;
			break;
		}
		}//end switch
	}
	//end if

	//giro derecha
	if (direccion == 1)
	{
		switch (modo) {
		case 0:
		{
			//pos_X ++;
			pos_Y++;
			break;
		}

		case 1:
		{
			pos_X--;
			//pos_Y ++;
			break;
		}

		case 2:
		{
			//pos_X --;
			pos_Y--;
			break;
		}

		case 3:
		{
			pos_X++;
			//pos_Y --;
			break;
		}
		}//end switch
	}
	//end if

	//recto
	if (direccion == 3)
	{
		switch (modo) {
		case 0:
		{
			pos_X++;

			break;
		}

		case 1:
		{

			pos_Y++;
			break;
		}

		case 2:
		{
			pos_X--;

			break;
		}

		case 3:
		{

			pos_Y--;
			break;
		}
		}//end switch
	}
	//end if

	//180
	if (direccion == 4)
	{
		switch (modo) {
		case 0:
		{
			pos_X--;
			break;
		}

		case 1:
		{
			pos_Y--;
			break;
		}

		case 2:
		{
			pos_X++;
			break;
		}

		case 3:
		{
			pos_Y++;
			break;
		}
		}//end switch
	}
	//end if

}

/*
 *	Indica la orientacion del robot
 */
void actualizar_brujula(uint8_t accion, int8_t cur_bruj) {
	switch (accion) {
	case 0:
	{
		break;
	}
	case 1:
	{
		cur_bruj = cur_bruj + 1;
		if (cur_bruj < 0) {
			cur_bruj = 3;
		}
		if (cur_bruj > 3) {
			cur_bruj = 0;
		}
		brujula = cur_bruj;
		break;
	}
	case 2:
	{
		cur_bruj = cur_bruj - 1;
		if (cur_bruj < 0) {
			cur_bruj = 3;
		}
		if (cur_bruj > 3) {
			cur_bruj = 0;
		}
		brujula = cur_bruj;
		break;
	}
	case 3:
	{
		switch (cur_bruj) {
		case 0:
			brujula = 2;
			break;
		case 1:
			brujula = 3;
			break;
		case 2:
			brujula = 0;
			break;
		case 3:
			brujula = 1;
			break;
		}
	}
	}
}


/* draw_walls
 * completa los campos de map_state de la celda actual con la informacion de los sensores,
 * a cada pared se le asigna un sensor en funcion del estado de brujula
 */
void draw_walls(uint8_t modo, uint8_t left, uint8_t right, uint8_t front) {
	switch (modo) {
	case 0:
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_NORTE, left);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_SUR, right);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_ESTE, front);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_OESTE, 0);
		break;
	case 1:
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_NORTE, 0);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_SUR, front);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_ESTE, left);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_OESTE, right);
		break;
	case 2:
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_NORTE, right);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_SUR, left);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_ESTE, 0);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_OESTE, front);
		break;
	case 3:
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_NORTE, front);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_SUR, 0);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_ESTE, right);
		BIT_WRITE((map_state[pos_X][pos_Y]), PARED_OESTE, left);
		break;
	}
}


/*
 *	Detecta la presencia de las paredes de la celda actual
 */
void has_wall_presence(void)
{
	get_adc();
	if (left_sensor < 2000)
		wall_presence.hasLeftWall = true;
	else
		wall_presence.hasLeftWall = false;

	if (right_sensor < 2000)
		wall_presence.hasRightWall = true;
	else
		wall_presence.hasRightWall = false;

	if (front_sensor < 2500) {
		wall_presence.hasFrontWall = true;				//podria agregarse una actualizacion de total_distance para los giros o llamar a funcion calibracion frontal
	}
	else
		wall_presence.hasFrontWall = false;
}






//-----------------------------------------------------------------------------------------------






/******* end of file *******/
